<?php get_header(); ?>
			
			<?php

			/* $category_array is also used to indicate which post NOT to display in other loops on this page */
			$category_array = return_ids_of_category_array(array('Homepage Slider')); // Get id of categories used in slider

			$use_carousel = of_get_option('showhidden_slideroptions');
			if ($use_carousel) {
				global $post;
				$tmp_post = $post;
				$show_posts = of_get_option('slider_options');
				$args = array( 'numberposts' => $show_posts, 'category__in' => $category_array ); // set this to how many posts you want in the carousel
				$myposts = get_posts( $args );
				$post_num = 0;
				$slide_num = 0;
				$isFirst = true;
			?>
				<?php if(!empty($myposts)) { ?>
					<div id="myCarousel" class="carousel slide">
						
						<?php
						// If post assigned to Homepage Slider is missing featured image then invoke error by setting variable
						foreach($myposts as $key => $value) {
							if(!has_post_thumbnail($value->ID) && !isset($freezeSlider)) { // If post does not have image assign
								$freezeSlider = 'yes';
							}
						}
						?>

					    <!-- Carousel items -->
					    <div class="carousel-inner">

					    	<?php
							foreach( $myposts as $post ) :	setup_postdata($post);
								if(!has_post_thumbnail($post->ID)) {
									echo '<br /><div class="alert alert-error">
									        <h4 class="alert-heading">Error: "' . $post->post_title . '" post is missing the featured image!</h4>
									        <br />
									        <p>The slider will not work until this is fixed.</p>
									      </div>';
								} else {
									$post_num++;
									$post_thumbnail_id = get_post_thumbnail_id();
									$featured_src = wp_get_attachment_image_src( $post_thumbnail_id, 'wpbs-featured-carousel' );
								}
							?>

						    <div class="<?php if($post_num == 1){ echo 'active'; } ?> item">
						    	<a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_post_thumbnail( 'wpbs-featured-carousel' ); ?></a>

							   	<div class="carousel-caption">

					                <h4><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h4>
					                <p>
					                	<?php
					                	if(has_post_thumbnail($post->ID)) { // Only run code below if post has thumbnail
					                		$excerpt_length = 100; // length of excerpt to show (in characters)
					                		$the_excerpt = get_the_excerpt(); 
					                		if($the_excerpt != ""){
					                			$the_excerpt = substr( $the_excerpt, 0, $excerpt_length );
					                			echo $the_excerpt . '... ';
					                	?>
					                	<a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>" class="btn btn-primary">Read more &rsaquo;</a>
					                		<?php } ?>
					                	<?php } ?>
					                </p>

				                </div>
						    </div>

						    <?php endforeach; ?>
							<?php $post = $tmp_post; ?>

					    </div>

					    <!-- Carousel nav -->
					    <?php if(!is_admin() && !isset($freezeSlider)): // Only display slider navigation when no error with posts?>
						    <a class="carousel-control left" href="#myCarousel" data-slide="prev">&lsaquo;</a>
						    <a class="carousel-control right" href="#myCarousel" data-slide="next">&rsaquo;</a>
							
							<script type="text/javascript">
								jQuery(document).ready(function($) {
								    $('.carousel').carousel({
								  		interval: 7000	
								  	})
								});
							</script>
						<?php endif; ?>
				    </div>
				<?php } else { ?>
					<div class="alert alert-error">
				        <h4 class="alert-heading">Error:</h4>
				        <p>Homepage Slider is enabled but no posts were assigned! The slider will not work until this is fixed.</p>
				    </div>
				<?php } ?>
			<?php } else { ?>
		    	<div class="clearfix row-fluid">
		    		<div class="hero-unit">
		    			<h1><?php bloginfo('title'); ?></h1>
		    			<p><?php bloginfo('description'); ?></p>
		    		</div>
		    	</div>
		    <?php }// ends the if use carousel statement ?>
			
			<div id="content" class="clearfix row-fluid">
				
				<div id="main" class="span8 clearfix" role="main">

					<?php $args = array('post_type' => array('post', 'document'), 'category__not_in' => $category_array);  ?>

					<?php $q = new WP_Query( $args );?>

					<?php if ($q->have_posts()) : while ($q->have_posts()) : $q->the_post(); ?>

						<article id="post-<?php the_ID(); ?>" <?php post_class('clearfix'); ?> role="article" style="margin-bottom:0px !important">
			
							<header id="<?php echo $tag_list = commaSeparatedTagList($q->post->ID); ?>">

								<h3><?php the_title(); ?></h3>
								
								<small style="color:#b4bcc2;">Posted on <?php echo $date = formatModifiedDate($q->post->post_modified); ?></small>
							
							</header> <!-- end article header -->
							
							<section class="post_content">
								
								<h5 style="margin:0px;"><?php echo substr(strip_tags($post->post_content), 0, 300);?> ...</h5>
							
							</section> <!-- end article section -->
							
							<footer>

								<div><a class="btn btn-small" href="<?php echo get_permalink(); ?>">Read more <i class="icon-chevron-right icon-white"></i></a></div>
							
							</footer> <!-- end article footer -->
						
						</article> <!-- end article -->
					<?php endwhile; ?>	
					
					<?php if (function_exists('page_navi')) { // if expirimental feature is active ?>
						
						<?php page_navi(); // use the page navi function ?>
						
					<?php } else { // if it is disabled, display regular wp prev & next links ?>
						<nav class="wp-prev-next">
							<ul class="clearfix">
								<li class="prev-link"><?php next_posts_link(__('&laquo; Older Entries', "bonestheme")) ?></li>
								<li class="next-link"><?php previous_posts_link(__('Newer Entries &raquo;', "bonestheme")) ?></li>
							</ul>
						</nav>
					<?php } ?>		
					
					<?php else : ?>
					
					<article id="post-not-found">
					    <header>
					    	<h1><?php _e("Not Found", "bonestheme"); ?></h1>
					    </header>
					    <section class="post_content">
					    	<p><?php _e("Sorry, but the requested resource was not found on this site.", "bonestheme"); ?></p>
					    </section>
					    <footer>
					    </footer>
					</article>
					
					<?php endif; ?>
			
				</div> <!-- end #main -->
    
				<?php get_sidebar(); // sidebar 1 ?>

				<div id="sidebar1" class="fluid-sidebar sidebar span4" role="complementary">
					<h4 class="widgettitle">Recently updated files</h4>
					<?php $docargs = array(); ?>
		    		<?php $docRevisions = get_documents($docargs); #print_r($docRevisions);?>
					<?php foreach($docRevisions as $docRevision): ?>
						<dl>
						<dt id="file-name"><strong><?php echo $docRevision->post_title; ?></strong> <a href="<?php echo $docRevision->guid; ?>/<?php echo $docRevision->post_name; ?>"><i class="icon-download-alt"></i></a></dt>
						<dd id="revision-desc" class="text-info"><small><?php echo $docRevision->post_excerpt; ?></small></dd>
						<dd id="revision-modified" class="muted"><small><em>Modified on <?php echo $date = formatModifiedDate($docRevision->post_modified); ?></em></small></dd>
						</dl>
					<?php endforeach; ?>
				</div>
    
			</div> <!-- end #content -->

<?php get_footer(); ?>